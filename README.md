```
$ nospoiler https://www.youtube.com/watch?v=gLU9NNrJBd0
```

For now, it ignores explicit starting time:
```
$ nospoiler https://www.youtube.com/watch?v=3dQiFiQUJ8Y&t=16m11s
```
