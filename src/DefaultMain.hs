module DefaultMain (
  defaultMain
  ) where

import Process.Chrome
import Youtube.URL
import System.Environment

usage :: IO ()
usage = do
  putStrLn "Usage:"
  putStrLn "$ nospoiler \"https://www.youtube.com/watch?v=gLU9NNrJBd0\""

defaultMain :: IO ()
defaultMain =
  getArgs >>= \x -> case x of
    [argUrl] -> viewUrl argUrl 
    _ -> usage


viewUrl :: String -> IO ()
viewUrl surl = case embeddedURL surl of
  Just url -> openURL url
  _ -> do putStrLn ("Could not parse " ++ surl)
          usage
