{-# LANGUAGE RecordWildCards #-}
module DefaultMain (
  main
  ) where

import           Lens.Micro.Platform        (over, set)
import           Data.Maybe
import           Data.Monoid
import           Debug.Trace
import           Network.URL
import           Options.Applicative
import           Process.Brave
import           Safe
import           System.Clipboard
import           System.Environment
import           System.Process
import           Timestamp
import           Youtube.URL

data Opts = Opts {
  _url            :: Maybe String
  , _start        :: Maybe Timestamp
  , _useClipboard :: Bool
  } deriving Show

optsParser :: Parser Opts
optsParser = Opts <$>
  (optional $ strOption (
  metavar "URL"
  <> help "target url"
  ))
  <*>
  (fmap (>>= readTimestamp) $ optional $ strOption (
      metavar "TIMESTAMP"
      <> short 's'
      <> long "start"
      <> help "Starting time using format (int)h(int)m(int)s. Order does not matter"
  )) <*>
  switch (
  long "clipboard"
  <> help "Whether to use to url in the clibpoard"
  )


usage :: IO ()
usage = do
  putStrLn "Usage:"
  putStrLn "$ nospoiler \"https://www.youtube.com/watch?v=gLU9NNrJBd0\""

main :: IO ()
main = execParser opts >>= mainWithOpts
  where
    opts = info (helper <*> optsParser)
      (fullDesc
       <> header "Open a URL in embedded mode")

mainWithOpts :: Opts -> IO ()
mainWithOpts opts = getUrl opts >>= viewUrl (_start opts)

getUrlMaybe :: Opts -> IO (Maybe String)
getUrlMaybe Opts{..}
  | _useClipboard = getClipboardString
  | otherwise = traceM (show _url) >> return _url

getUrl :: Opts -> IO String
getUrl opts = do
  ms <- getUrlMaybe opts
  case ms of
    Nothing -> fail "No url was provided"
    Just s  -> return s

viewUrl :: Maybe Timestamp -> String -> IO ()
viewUrl start surl = case getVideoInfo surl of
  Just vinfo ->
    openURL (embedVideoInfo vinfo') -- for Firefox or Crhome
    where
      vinfo' = maybe vinfo (\ts -> set timestamp (Just ts) vinfo) start
  _ -> do putStrLn msg
          spawnProcess "rofi" ["-e", msg]
          usage
           where msg = "Could not parse " ++ surl
