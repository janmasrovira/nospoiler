-- | Examples:
--
-- >>> (readTimestamp "1h1h1s3m3m") == readTimestamp "2h6m1s"
-- True
--
-- >>> (readTimestamp "3600s3600s" == readTimestamp "1h60m")
-- True

module Timestamp (
  Timestamp
  , timestampReadP
  , timestampZero
  , readTimestamp
  , seconds
  ) where

import Data.Ix
import Data.Maybe
import Text.ParserCombinators.ReadP as P

newtype Timestamp = Timestamp Int
  deriving (Show, Eq, Num, Ord, Real, Enum)

timestampReadP :: ReadP Timestamp
timestampReadP = Timestamp . sum <$> many term
    where
      isDigit :: Char -> Bool
      isDigit = inRange ('0', '9')

      uint :: ReadP Int
      uint = read <$> munch1 isDigit

      hms :: ReadP (Int -> Int)
      hms = P.choice [ char 'h' >> return (*3600)
                     , char 'm' >> return (*60)
                     , char 's' >> return id]

      term :: ReadP Int
      term = do
        n <- uint
        f <- hms
        return (f n)

timestampZero :: Timestamp
timestampZero = Timestamp 0

seconds :: Timestamp -> Int
seconds (Timestamp s) = s

readTimestamp :: String -> Maybe Timestamp
readTimestamp = fmap fst . listToMaybe . filter (null.snd) . readP_to_S timestampReadP
