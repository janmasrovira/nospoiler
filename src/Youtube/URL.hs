{-# LANGUAGE TemplateHaskell #-}
-- | Examples:
--
-- >>> fmap exportURL $ embeddedURL "https://www.youtube.com/watch?v=lasWefVUCsI"
-- Just "https://www.youtube.com/embed/lasWefVUCsI?controls=0&start=0&autoplay=1"
--
-- >>> fmap exportURL $ embeddedURL "https://www.youtube.com/watch?v=lasWefVUCsI&t=5h3m15s"
-- Just "https://www.youtube.com/embed/lasWefVUCsI?controls=0&start=18195&autoplay=1"
--
-- This kind of URL's are not yet supported:
--
-- >>> fmap exportURL $ embeddedURL "https://youtu.be/lasWefVUCsI?t=5h56m3s"
-- Nothing

module Youtube.URL (
  embedVideoInfo
  , getVideoInfo
  , VideoInfo(..)
  , vidId
  , timestamp
  , embeddedURL
  ) where


import Lens.Micro.Platform
import Control.Monad
import Data.Maybe
import Network.URL
import Timestamp

type UrlHandler = URL -> Maybe VideoInfo

data VideoInfo = VideoInfo {
  _vidId :: String
  , _timestamp :: Maybe Timestamp
  } deriving (Show)
makeLenses ''VideoInfo

argVidId :: String
argVidId = "v"

argTimestamp :: String
argTimestamp = "t"

argStart :: String
argStart = "start"

argControls :: String
argControls = "controls"

argAutoPlay :: String
argAutoPlay = "autoplay"

embeddedURL :: String -> Maybe URL
embeddedURL = fmap embedVideoInfo . getVideoInfo

getVideoInfo :: String -> Maybe VideoInfo
getVideoInfo str = do
  params <- importURL str
  listToMaybe $ catMaybes
    [ f params | f <- urlHandlers ]

urlHandlers :: [UrlHandler]
urlHandlers = [normalHandler, shortHandler]

-- | for urls such as youtu.be/vS5ntTOTjGg&t=7s
shortHandler :: URL -> Maybe VideoInfo
shortHandler url = do
  checkHost
  let (_vidId, rest) = span (/= '&') path
      strTimestamp = drop 3 rest -- drops "&t="
      _timestamp = readTimestamp strTimestamp
  return VideoInfo {..}
  where
  path = url_path url
  checkHost = case url_type url of
    Absolute h -> guard (host h == "youtu.be")
    _ -> return ()

-- | for urls such as www.youtube.com/watch?v=lasWefVUCsI
normalHandler :: URL -> Maybe VideoInfo
normalHandler url = do
  vid <- lookup argVidId params
  let ts = lookup argTimestamp params >>= readTimestamp
  return VideoInfo {_vidId = vid
                   , _timestamp = ts}
  where
    params = url_params url

embedVideoInfo :: VideoInfo -> URL
embedVideoInfo VideoInfo{..} = URL {
  url_type = Absolute Host {
      protocol = HTTP True
      , host = "www.youtube.com"
      , port = Nothing}
  , url_path = "embed/" ++ _vidId
  , url_params = [(argControls, "0")
                 , (argStart, show $ seconds $ fromMaybe timestampZero _timestamp)
                 , (argAutoPlay, "1")]}
