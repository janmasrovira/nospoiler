-- | Examples:
--
-- >>> (read "1h1h1s3m3m" :: TimeStamp) == read "2h6m1s"
-- True
--
-- >>> (read "" :: TimeStamp) + 1
-- TimeStamp 1
--
-- >>> fmap exportURL $ embeddedURL "https://www.youtube.com/watch?v=lasWefVUCsI"
-- Just "https://www.youtube.com/embed/lasWefVUCsI?controls=0&start=0&autoplay=1"
--
-- >>> fmap exportURL $ embeddedURL "https://www.youtube.com/watch?v=lasWefVUCsI&t=5h3m15s"
-- Just "https://www.youtube.com/embed/lasWefVUCsI?controls=0&start=18195&autoplay=1"
--
-- This kind of URL's are not yet supported:
--
-- >>> fmap exportURL $ embeddedURL "https://youtu.be/lasWefVUCsI?t=5h56m3s"
-- Nothing

module Youtube.URL (
  embedVideoInfo
  , getVideoInfo
  , VideoInfo(..)
  , TimeStamp
  , embeddedURL
  ) where


import Data.Default
import Data.Ix
import Data.Maybe
import Network.URL
import Text.ParserCombinators.ReadP as P
import Text.Read

newtype TimeStamp = TimeStamp Int deriving (Show, Eq, Num, Ord, Real, Enum)


-- | Parser for 'TimeStamp'.
--
-- Note that it ignores the order of the terms and adds them,
-- so 1h1h1s3m3m is equivalent to 2h6m1s.
instance Read TimeStamp where
  readPrec = lift $
    TimeStamp . sum <$> many term
    where
      isDigit :: Char -> Bool
      isDigit = inRange ('0', '9')
      
      uint :: ReadP Int
      uint = read <$> munch1 isDigit
      
      hms :: ReadP (Int -> Int)
      hms = P.choice [ char 'h' >> return (*3600)
                     , char 'm' >> return (*60)
                     , char 's' >> return id]

      term :: ReadP Int
      term = do
        n <- uint
        f <- hms
        return (f n)

instance Default TimeStamp where
  def = TimeStamp 0


data VideoInfo = VideoInfo {
  _vidId :: String
  , _timeStamp :: Maybe TimeStamp
  }


_seconds :: TimeStamp -> Int
_seconds (TimeStamp s) = s 


argVidId :: String
argVidId = "v"

argTimeStamp :: String
argTimeStamp = "t"

argStart :: String
argStart = "start"

argControls :: String
argControls = "controls"

argAutoPlay :: String
argAutoPlay = "autoplay"

embeddedURL :: String -> Maybe URL
embeddedURL = fmap embedVideoInfo . getVideoInfo

getVideoInfo :: String -> Maybe VideoInfo
getVideoInfo url = do
  params <- url_params <$> importURL url
  vid <- lookup argVidId params
  let ts = read <$> lookup argTimeStamp params
  return VideoInfo {_vidId = vid
                   , _timeStamp = ts}


embedVideoInfo :: VideoInfo -> URL
embedVideoInfo VideoInfo{..} = URL {
  url_type = Absolute Host {
      protocol = HTTP True
      , host = "www.youtube.com"
      , port = Nothing}
  , url_path = "embed/" ++ _vidId
  , url_params = [(argControls, "0")
                 , (argStart, show $ _seconds $ fromMaybe def _timeStamp)
                 , (argAutoPlay, "1")]}
