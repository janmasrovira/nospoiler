module Process.Brave (
  openURL
  ) where

import Network.URL
import System.Process

brave :: String
brave = "brave"

openURL :: URL -> IO ()
openURL u = callProcess brave ["-new-window", exportURL u]
