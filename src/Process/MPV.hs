module Process.MPV (
  openURL
  ) where

import Network.URL
import System.Process

-- I have to use the ubuntu package version since the version compiled from
-- source in /usr/local/bin does not properly work for youtube videos.
mpvCmd :: String
mpvCmd = "mpv"

openURL :: URL -> IO ()
openURL u = callProcess mpvCmd
  ["--ytdl-format=bestvideo[height<=?1440]+bestaudio/best"
  , exportURL u]
