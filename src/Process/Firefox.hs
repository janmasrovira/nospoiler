module Process.Firefox (
  openURL
  ) where

import Network.URL
import System.Process

firefoxCmd :: String
firefoxCmd = "firefox"

openURL :: URL -> IO ()
openURL u = callProcess firefoxCmd ["-new-window", exportURL u]
