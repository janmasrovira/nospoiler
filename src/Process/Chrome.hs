module Process.Chrome (
  openURL
  ) where

import Network.URL
import System.Process

chromeCmd :: String
chromeCmd = "google-chrome"

openURL :: URL -> IO ()
openURL u = callProcess chromeCmd [exportURL u]
