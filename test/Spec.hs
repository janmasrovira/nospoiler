module Main (
  main) where


import Test.DocTest

extensions :: [String]
extensions = ["RecordWildCards"
             , "GeneralizedNewtypeDeriving"
             ]

main :: IO ()
main = do
  putStrLn "DocTest:"
  doctest $ map ("-X"++) extensions ++ ["src/"]
  
